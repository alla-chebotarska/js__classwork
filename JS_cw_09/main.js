let counter = 0;
let intervalHandler;

const getElement = function(id){
    return document.getElementById(id);
}

const count = () => {
    getElement("output").innerHTML = counter;
    counter++;
}

getElement("StartBtn").onclick = () => {
    intervalHandler = setInterval(count, 1000);
}

getElement("StopBtn").onclick = () => {
    clearInterval(intervalHandler);
}

getElement("CleanBtn").onclick = () => {
    counter = 0;
}



