const numberBtns = document.getElementsByClassName("number-btn");
const operatorBtns = document.getElementsByClassName("operator-btn");
const screen = document.getElementById("screen");
const equalBtn = document.getElementById("equallBtn");
const ACBtn = document.getElementById("ACBtn");

const onACBtnClick = function (event) {
    screen.innerText = "0";
}

ACBtn.addEventListener("click", onACBtnClick);


const onNumberBtnClick = function (event) {
    if (screen.innerText === "0") {
        screen.innerText = event.target.value;
    } else {
        screen.innerText += event.target.value;
    }
}

for (let button of numberBtns) {
    button.addEventListener("click", onNumberBtnClick);
}

const isSign = function (symbol) {
    return symbol == "+" || symbol == "-" || symbol == "*" || symbol == "/";
}

const onOperatorBtnClick = function (event) {
    let lastSymbol = screen.innerText[screen.innerText.length - 1];
    if (isSign(lastSymbol)) {
        screen.innerText = screen.innerText.substring(0, screen.innerText.length - 1) + event.target.value;
    } else {
        screen.innerText += event.target.value;
    }
}

for (let operatorBtn of operatorBtns) {
    operatorBtn.addEventListener("click", onOperatorBtnClick);
}

const onEqualBtnClick = function (event) {
    let calc = new Calculator();
    let res = calc.calculate(screen.innerText);
    screen.innerText = res;
    console.log(res);
}

equalBtn.addEventListener("click", onEqualBtnClick);


class Calculator {
    stack = [];
    operatorsMap = new Map([['+', 1], ['-', 1], ['*', 2], ['/', 2]]);

    calculate(expression) {
        let startPos = 0;
        for (let i = 0; i < expression.length; i++) {
            if (isSign(expression[i])) {
                this.stack.push(parseFloat(expression.substring(startPos, i)));
                this.processSign(expression.substring(i, i + 1));
                startPos = i + 1;
            }
        }
        this.stack.push(parseFloat(expression.substring(startPos, expression.length)));

        while (this.stack.length >= 3) {
            console.log(this.stack);
            let secondOperand = this.stack.pop();
            let sing = this.stack.pop();
            let firstOpperand = this.stack.pop();
            this.stack.push(this.executeOpperation(firstOpperand, sing, secondOperand));
        }
        return this.stack[0];
    }

    processSign(symbol) {
        if (this.stack.length < 2) {
            this.stack.push(symbol);
            return;
        }
        let currentOperatorPriority = this.operatorsMap.get(symbol);
        let previousOperetoRPriority = this.operatorsMap.get(this.stack[this.stack.length - 2]);

        if (currentOperatorPriority <= previousOperetoRPriority) {
            let secondOperand = this.stack.pop();
            let sing = this.stack.pop();
            let firstOpperand = this.stack.pop();
            this.stack.push(this.executeOpperation(firstOpperand, sing, secondOperand));
        }
        this.stack.push(symbol);

    }

    executeOpperation(firstOpperand, sign, secondOperand) {
        switch (sign) {
            case "+":
                return firstOpperand + secondOperand;
                break;
            case "-":
                return firstOpperand - secondOperand;
                break;
            case "*":
                return firstOpperand * secondOperand;
                break;
            case "/":
                return firstOpperand / secondOperand;
                break;

        }
    }
}

