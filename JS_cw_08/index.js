const exchangeRate = {"date":"14.09.2020","bank":"PB","baseCurrency":980,"baseCurrencyLit":"UAH","exchangeRate":  
                    [{"baseCurrency":"UAH","saleRateNB":20.3588000,"purchaseRateNB":20.3588000},
                    {"baseCurrency":"UAH","currency":"AZN","saleRateNB":16.4235000,"purchaseRateNB":16.4235000},
                    {"baseCurrency":"UAH","currency":"BYN","saleRateNB":10.7375000,"purchaseRateNB":10.7375000},
                    {"baseCurrency":"UAH","currency":"CAD","saleRateNB":21.1799000,"purchaseRateNB":21.1799000},
                    {"baseCurrency":"UAH","currency":"CHF","saleRateNB":30.6765000,"purchaseRateNB":30.6765000},
                    {"baseCurrency":"UAH","currency":"CNY","saleRateNB":4.0814000,"purchaseRateNB":4.0814000},
                    {"baseCurrency":"UAH","currency":"CZK","saleRateNB":1.2447000,"purchaseRateNB":1.2447000},
                    {"baseCurrency":"UAH","currency":"DKK","saleRateNB":4.4444000,"purchaseRateNB":4.4444000},
                    {"baseCurrency":"UAH","currency":"EUR","saleRateNB":33.0674000,"purchaseRateNB":33.0674000},
                    {"baseCurrency":"UAH","currency":"GBP","saleRateNB":35.7821000,"purchaseRateNB":35.7821000},
                    {"baseCurrency":"UAH","currency":"HUF","saleRateNB":0.0925660,"purchaseRateNB":0.0925660},
                    {"baseCurrency":"UAH","currency":"ILS","saleRateNB":8.0595000,"purchaseRateNB":8.0595000},
                    {"baseCurrency":"UAH","currency":"JPY","saleRateNB":0.2628300,"purchaseRateNB":0.2628300},
                    {"baseCurrency":"UAH","currency":"KZT","saleRateNB":0.0653570,"purchaseRateNB":0.0653570},
                    {"baseCurrency":"UAH","currency":"MDL","saleRateNB":1.6776000,"purchaseRateNB":1.6776000},
                    {"baseCurrency":"UAH","currency":"NOK","saleRateNB":3.0904000,"purchaseRateNB":3.0904000},
                    {"baseCurrency":"UAH","currency":"PLZ","saleRateNB":7.4329000,"purchaseRateNB":7.4329000},
                    {"baseCurrency":"UAH","currency":"RUB","saleRateNB":0.3720300,"purchaseRateNB":0.3720300},
                    {"baseCurrency":"UAH","currency":"SEK","saleRateNB":3.1869000,"purchaseRateNB":3.1869000},
                    {"baseCurrency":"UAH","currency":"SGD","saleRateNB":20.4173000,"purchaseRateNB":20.4173000},
                    {"baseCurrency":"UAH","currency":"TMT","saleRateNB":7.8748000,"purchaseRateNB":7.8748000},
                    {"baseCurrency":"UAH","currency":"TRY","saleRateNB":3.7345000,"purchaseRateNB":3.7345000},
                    {"baseCurrency":"UAH","currency":"UAH","saleRateNB":1.0000000,"purchaseRateNB":1.0000000},
                    {"baseCurrency":"UAH","currency":"USD","saleRateNB":27.9003000,"purchaseRateNB":27.9003000},
                    {"baseCurrency":"UAH","currency":"UZS","saleRateNB":0.0026856,"purchaseRateNB":0.0026856},
                    {"baseCurrency":"UAH","currency":"GEL","saleRateNB":8.9644000,"purchaseRateNB":8.9644000}]}; // The exchange rate for 14 September 2020

const kurs = document.getElementById("root");
kurs.innerHTML = `${exchangeRate.exchangeRate[3].currency} ${exchangeRate.exchangeRate[3].saleRateNB} ${exchangeRate.exchangeRate[3].purchaseRateNB} </br></br>
                  ${exchangeRate.exchangeRate[8].currency} ${exchangeRate.exchangeRate[8].saleRateNB} ${exchangeRate.exchangeRate[8].purchaseRateNB} </br></br>
                  ${exchangeRate.exchangeRate[11].currency} ${exchangeRate.exchangeRate[11].saleRateNB} ${exchangeRate.exchangeRate[11].purchaseRateNB} </br></br>
                  ${exchangeRate.exchangeRate[13].currency} ${exchangeRate.exchangeRate[13].saleRateNB} ${exchangeRate.exchangeRate[13].purchaseRateNB} </br></br>
                  ${exchangeRate.exchangeRate[17].currency} ${exchangeRate.exchangeRate[17].saleRateNB} ${exchangeRate.exchangeRate[17].purchaseRateNB} </br></br>
                  ${exchangeRate.exchangeRate[23].currency} ${exchangeRate.exchangeRate[23].saleRateNB} ${exchangeRate.exchangeRate[23].purchaseRateNB} </br></br>`;